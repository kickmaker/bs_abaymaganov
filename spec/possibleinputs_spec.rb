require 'coordinates'
require 'possibleinputs'

class PlayerPossibleInputs
  include PossibleInputs::Player
end

describe PlayerPossibleInputs do
  before(:all) do
    @expected_values = Hash.new

    (0..9).each do |x|
      (0..9).each do |y|
        key = ('A'.ord + x).chr + (y + 1).to_s
        @expected_values[key] = Coordinates.new(x, y)
      end
    end
  end

  context 'when player_possible_inputs calls' do
    it 'must return hash with pairs (key, value) like [:A1 => (0, 0), etc]' do
      possible_inputs = PlayerPossibleInputs.new().player_possible_inputs
      expect(possible_inputs).to be == @expected_values
    end
  end
end
