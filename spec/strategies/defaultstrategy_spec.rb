require 'coordinates'
require 'board'
require 'strategies/basestrategy'
require 'strategies/defaultstrategy'

describe DefaultStrategy do
  describe '#places' do
    context 'when positions include gap places for sheep' do
      it 'should return empty places' do
        positions = [Coordinates.new(1, 1), Coordinates.new(1, 2), Coordinates.new(1, 4)]
        decks = 4
        strategy = DefaultStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty
      end
    end

    context 'when positions count more or equal than decks count' do
      it 'should return empty places' do
        positions = [Coordinates.new(1, 1), Coordinates.new(1, 2)]
        decks = positions.count
        strategy = DefaultStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty

        decks = positions.count - 1
        strategy = DefaultStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty
      end
    end

    context 'when positions is empty and decks count greater than zero' do
      it 'should return places that includes all coordinates of the field' do
        field_places = Array.new(10) do |x|
          Array.new(10) do |y|
            Coordinates.new(x, y)
          end
        end.flatten(1)

        strategy = DefaultStrategy.new([], 1)
        places = strategy.places
        expect(places).to be == field_places
      end
    end

    context 'when positions count equal 1, decks count more than 1, ship is located in center' do
      it 'should return 4 places: left, right, top and bottom coordinates' do
        position = Coordinates.new(3, 3)
        top = Coordinates.new(position.x, position.y - 1)
        bottom = Coordinates.new(position.x, position.y + 1)
        left = Coordinates.new(position.x - 1, position.y)
        right = Coordinates.new(position.x + 1, position.y)

        strategy = DefaultStrategy.new([position], 2)
        places = strategy.places
        expect(places.count).to be == 4
        expect(places).to include top
        expect(places).to include bottom
        expect(places).to include left
        expect(places).to include right
      end
    end

    context 'when positions count == 1, decks count > 1, ship is located in left top corner' do
      it 'should return 2 places: right and bottom coordinates' do
        position = Coordinates.new(0, 0)
        bottom = Coordinates.new(position.x, position.y + 1)
        right = Coordinates.new(position.x + 1, position.y)

        strategy = DefaultStrategy.new([position], 2)
        places = strategy.places
        expect(places.count).to be == 2
        expect(places).to include bottom
        expect(places).to include right
      end
    end

    context 'when positions count == 1, decks count > 1, ship is located at the left border' do
      it 'should return 3 places: right, top and bottom coordinates' do
        position = Coordinates.new(0, 1)
        top = Coordinates.new(position.x, position.y - 1)
        bottom = Coordinates.new(position.x, position.y + 1)
        right = Coordinates.new(position.x + 1, position.y)

        strategy = DefaultStrategy.new([position], 2)
        places = strategy.places
        expect(places.count).to be == 3
        expect(places).to include top
        expect(places).to include bottom
        expect(places).to include right
      end
    end

    context 'when positions count > 1, decks count < positions count, vertical ship is located in center' do
      it 'should return 2 places: top and bottom' do
        positions = [Coordinates.new(3, 3), Coordinates.new(3, 4)]
        top = Coordinates.new(3, 2)
        bottom = Coordinates.new(3, 5)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 2
        expect(places).to include top
        expect(places).to include bottom
      end
    end

    context 'when positions count > 1, decks count < positions count, vertical ship is located at the left border' do
      it 'should return 2 places: top and bottom' do
        positions = [Coordinates.new(0, 1), Coordinates.new(0, 2)]
        top = Coordinates.new(0, 0)
        bottom = Coordinates.new(0, 3)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 2
        expect(places).to include top
        expect(places).to include bottom
      end
    end

    context 'when positions count > 1, decks count < positions count, vertical ship is located in left top corner' do
      it 'should return 2 places: bottom' do
        positions = [Coordinates.new(0, 0), Coordinates.new(0, 1)]
        bottom = Coordinates.new(0, 2)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 1
        expect(places).to include bottom
      end
    end

    context 'when positions count > 1, decks count < positions count, horizontal ship is located in center' do
      it 'should return 2 places: left and right' do
        positions = [Coordinates.new(3, 3), Coordinates.new(4, 3)]
        left = Coordinates.new(2, 3)
        right = Coordinates.new(5, 3)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 2
        expect(places).to include left
        expect(places).to include right
      end
    end

    context 'when positions count > 1, decks count < positions count, horizontal ship is located at the left border' do
      it 'should return 1 places: right' do
        positions = [Coordinates.new(0, 1), Coordinates.new(1, 1)]
        right = Coordinates.new(2, 1)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 1
        expect(places).to include right
      end
    end

    context 'when positions count > 1, decks count < positions count, horizontal ship is located in left top corner' do
      it 'should return 1 places: right' do
        positions = [Coordinates.new(0, 0), Coordinates.new(1, 0)]
        right = Coordinates.new(2, 0)

        strategy = DefaultStrategy.new(positions, 3)
        places = strategy.places
        expect(places.count).to be == 1
        expect(places).to include right
      end
    end
  end
end
