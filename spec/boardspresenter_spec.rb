require 'boardspresenter'

describe BoardsPresenter do
  describe '#present' do
    before(:all) do
      Cell = Class.new
      Deck = Class.new

      Board = Class.new
      Board::ROWS = 10
      Board::COLS = 10

      ConsoleOutput = Class.new do
        @out = String.new

        def self.out
          @out
        end

        def self.print(str)
          @out << str
        end

        def self.puts(str)
          @out << str + "\n"
        end

        def self.clear
          @out.clear
        end
      end
    end

    let(:cols_cnt) { Board::COLS }
    let(:rows_cnt) { Board::ROWS }

    let(:end_line) { "\n" }

    # for building boards header in presenter, also it first line into ConsoleOutput.out
    let(:space_between) { '  ' }
    let(:half_header) do
      half_header = ' ' * rows_cnt.to_s.length
      (0..cols_cnt - 1).each { |i| half_header << ('A'.ord + i).chr }
      half_header
    end
    let(:expected_header) { half_header + space_between + half_header }

    # cells symbols from ConsoleOutput.out
    let(:plr_brd_out_col_A) { rows_cnt.to_s.length }
    let(:player_board_out_symbol_A1) { ConsoleOutput.out.lines(end_line)[1][plr_brd_out_col_A] }
    let(:player_board_out_symbol_A2) { ConsoleOutput.out.lines(end_line)[2][plr_brd_out_col_A] }
    let(:player_board_out_symbol_A3) { ConsoleOutput.out.lines(end_line)[3][plr_brd_out_col_A] }
    let(:player_board_out_symbol_A4) { ConsoleOutput.out.lines(end_line)[4][plr_brd_out_col_A] }

    let(:enm_brd_out_col_A) { (rows_cnt.to_s.length * 2) + space_between.length + cols_cnt }
    let(:enemy_board_out_symbol_A4) { ConsoleOutput.out.lines(end_line)[4][enm_brd_out_col_A] }
    let(:enemy_board_out_symbol_A5) { ConsoleOutput.out.lines(end_line)[5][enm_brd_out_col_A] }
    let(:enemy_board_out_symbol_A6) { ConsoleOutput.out.lines(end_line)[6][enm_brd_out_col_A] }
    let(:enemy_board_out_symbol_A7) { ConsoleOutput.out.lines(end_line)[7][enm_brd_out_col_A] }

    # cells doubles
    let(:unchecked_cell) { double(:class => Cell, :state => :unchecked) }
    let(:checked_cell) { double(:class => Cell, :state => :checked) }
    let(:unchecked_deck) { double(:class => Deck, :state => :unchecked) }
    let(:checked_deck) { double(:class => Deck, :state => :hit) }

    # "lets" for BoardsPresenter.new
    let(:player_board) do
      field = Array.new(cols_cnt * rows_cnt) { unchecked_cell }
      field[0] = unchecked_deck
      field[1] = checked_deck
      field[2] = checked_cell

      double(:player_board, :field => field)
    end

    let(:enemy_board) do
      field = Array.new(cols_cnt * rows_cnt) { unchecked_cell }
      field[3] = unchecked_deck
      field[4] = checked_deck
      field[5] = checked_cell

      double(:enemy_board, :field => field)
    end

    let(:presenter) { BoardsPresenter.new(player_board, enemy_board) }

    before(:each) { presenter.present }

    after(:each) { ConsoleOutput.clear}

    context 'when show boards' do
      it 'have header ABCDEFGHIJ' do
        presented_header = ConsoleOutput.out.lines(end_line)[0].chop
        expect(expected_header).to eq(presented_header)
      end

      it 'have 10 rows' do
        presented_rows_cnt = ConsoleOutput.out.lines(end_line).count - 1
        expect(presented_rows_cnt).to eq(rows_cnt)
      end

      context 'with player board' do
        it 'show \'.\' for unchecked empty cell' do
          expect(player_board_out_symbol_A4).to eq('.')
        end

        it 'show \'0\' for checked empty cell' do
          expect(player_board_out_symbol_A3).to eq('0')
        end

        it 'show \'s\' if deck was not hit' do
          expect(player_board_out_symbol_A1).to eq('s')
        end

        it 'show \'x\' if deck was hit' do
          expect(player_board_out_symbol_A2).to eq('x')
        end
      end

      context 'and enemy board is nil' do
        let(:presenter) { BoardsPresenter.new(player_board) }

        it 'have 10 columns' do
          column_summary = half_header.length

          ConsoleOutput.out.each_line(end_line) do |line|
            presented_line_len = line.length - end_line.length
            expect(presented_line_len).to eq(column_summary)
          end
        end
      end

      context 'and enemy board exist' do
        it 'have 10 columns per board' do
          column_summary = expected_header.length

          ConsoleOutput.out.each_line(end_line) do |line|
            presented_line_len = line.length - end_line.length
            expect(presented_line_len).to eq(column_summary)
          end
        end

        context 'and surrender is false' do
          it 'show \'.\' for unchecked cell' do
            expect(enemy_board_out_symbol_A7).to eq('.')
          end

          it 'show \'0\' for checked cell' do
            expect(enemy_board_out_symbol_A6).to eq('0')
          end

          it 'show \'.\' for unchecked deck' do
            expect(enemy_board_out_symbol_A4).to eq('.')
          end

          it 'show \'x\' if deck was hit' do
            expect(enemy_board_out_symbol_A5).to eq('x')
          end
        end

        context 'and surrender is true' do
          before(:each) do
            # to clear ConsoleOutput.out after outer before(:each)
            ConsoleOutput.clear

            presenter.present(surrender: true)
          end

          it 'show \'.\' for empty cell' do
            expect(enemy_board_out_symbol_A7).to eq('.')
          end

          it 'show \'0\' for checked empty cell' do
            expect(enemy_board_out_symbol_A6).to eq('0')
          end

          it 'show \'s\' if deck was not hit' do
            expect(enemy_board_out_symbol_A4).to eq('s')
          end

          it 'show \'x\' if deck was hit' do
            expect(enemy_board_out_symbol_A5).to eq('x')
          end
        end
      end
    end
  end
end
