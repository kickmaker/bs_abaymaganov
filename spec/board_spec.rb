require 'libloader'

describe Board do
  context 'when board filled' do
    it 'field should have 20 decks and 80 cells' do
      board = Board.new(input_type: Game::TYPE[:auto], placing_rules: Game::RULES[:default])
      allow(ConsoleOutput).to receive(:puts)
      board.fill_with_ships!
      field = board.field
      cells_cnt = decks_cnt = 0

      field.each do |cell|
        cells_cnt += 1 if cell.class == Cell
        decks_cnt += 1 if cell.class == Deck
      end

      expect(cells_cnt).to be == 80
      expect(decks_cnt).to be == 20
    end
  end

  context 'when use manual placing mode' do
    it 'should show message "Please enter coordinates for 4-decker ship"' do
      board = Board.new(input_type: Game::TYPE[:manual], placing_rules: Game::RULES[:default])
      board.instance_exec{ @ships = {4 => 1} }
      allow(ConsoleInput).to receive(:gets).and_return('A1', 'A2', 'A3', 'A4')
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      expect(ConsoleOutput).to receive(:puts).with('Please enter coordinates for 4-decker ship')
      board.fill_with_ships!
    end
  end

  context 'when ships overflow' do
    it 'should show message "Ship wasn\'t built"' do
      board = Board.new(input_type: Game::TYPE[:auto], placing_rules: Game::RULES[:default])
      board.instance_exec{ @ships = {4 => 1001} }
      expect {board.fill_with_ships!}.to raise_error('Ship wasn\'t built')
    end
  end

  context 'when call clean outer' do
    it 'should delete cells outside of the board' do
      field = Array.new(10) do |x|
        Array.new(10) do |y|
          Coordinates.new(x, y)
        end
      end.flatten(1)

      coordinates = Array[Coordinates.new(0, 0), Coordinates.new(1, 0),
                          Coordinates.new(0, 1), Coordinates.new(1, 1),
                          Coordinates.new(-1, -1), Coordinates.new(11, 11),]

      coordinates = Board::clean_outer(coordinates)
      coordinates.each do |coord|
        expect(field).to include coord
      end
    end
  end
end
