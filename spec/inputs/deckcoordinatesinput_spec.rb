require 'console_wrap/consoleinput'
require 'console_wrap/consoleoutput'
require 'inputs/someinput'
require 'inputs/userinput'
require 'inputs/deckcoordinatesinput'

describe DeckCoordinatesInput do
  before(:all) do
    @inputs = {A1: [1, 1], A2: [2, 2], B3: [3, 3]}
    @input = DeckCoordinatesInput.new(inputs: @inputs, choices: @inputs.values)
  end

  context 'when user enter correct deck number' do
    it 'should return value that have relationship with entered deck number' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleInput).to receive(:gets).and_return('A1')
      expect(@input.fetch).to be == @inputs[:A1]
    end
  end

  context 'when user enter wrong deck number' do
    it 'should print message "Incorrect coordinates, please enter coordinates in range A1-J10"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A3', 'A2')
      expect(ConsoleOutput).to receive(:puts).with('Incorrect coordinates, please enter coordinates in range A1-J10')
      @input.fetch
    end

    it 'must require correct input from user' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A3', 'B3')
      expect(@input.fetch).to be == @inputs[:B3]
    end
  end
end
