require 'console_wrap/consoleinput'
require 'console_wrap/consoleoutput'
require 'inputs/someinput'
require 'inputs/userinput'
require 'possibleinputs'
require 'game'
require 'inputs/gameoptionsinput'

describe GameOptionsInput do
  before(:all) do
    commands = Game::TYPE.merge(Game::RULES).merge(Game::START)
    @default_options = {input_type: Game::TYPE[:auto], placing_rules: Game::RULES[:default]}
    @input = GameOptionsInput.new(inputs: commands, case_sensitive: true, default_options: @default_options)
  end

  context 'when user enter "manual" command from console' do
    it 'should print message "Ship manual placing have been activated"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('manual', 'go')
      expect(ConsoleOutput).to receive(:puts).with('Ship manual placing have been activated')
      @input.fetch
    end

    it 'fetch should return hash with value for "manual" command key' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('manual', 'go')
      options = @input.fetch
      expect(options).to have_value(Game::TYPE[:manual])
    end
  end

  context 'when user enter "auto" command from console' do
    it 'should print message "Ship auto placing have been activated"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('auto', 'go')
      expect(ConsoleOutput).to receive(:puts).with('Ship auto placing have been activated')
      @input.fetch
    end

    it 'fetch should return hash with value for "auto" command key' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('auto', 'go')
      options = @input.fetch
      expect(options).to have_value(Game::TYPE[:auto])
    end
  end

  context 'when user enter "default" command from console' do
    it 'should print message "Standard game rules have been activated"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('default', 'go')
      expect(ConsoleOutput).to receive(:puts).with('Standard game rules have been activated')
      @input.fetch
    end

    it 'fetch should return hash with value for "default" command key' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('default', 'go')
      options = @input.fetch
      expect(options).to have_value(Game::RULES[:default])
    end
  end

  context 'when user enter "tetris" command from console' do
    it 'should print message "Tetris game rules have been activated"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('tetris', 'go')
      expect(ConsoleOutput).to receive(:puts).with('Tetris game rules have been activated')
      @input.fetch
    end

    it 'fetch should return hash with value for "tetris" command key' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('tetris', 'go')
      options = @input.fetch
      expect(options).to have_value(Game::RULES[:tetris])
    end
  end

  context 'when user enter unknown command from console' do
    it 'should print message "Unknown command, please repeat..."' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('any unknown command', 'go')
      expect(ConsoleOutput).to receive(:puts).with('Unknown command, please repeat...')
      @input.fetch
    end
  end

  context 'when init default game options and user enter "go"' do
    it 'fetch should return default game options"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleInput).to receive(:gets).and_return('go')
      expect(@input.fetch).to be == @default_options
    end
  end  
end
