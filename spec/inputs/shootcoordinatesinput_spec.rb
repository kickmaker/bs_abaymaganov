require 'console_wrap/consoleinput'
require 'console_wrap/consoleoutput'
require 'inputs/someinput'
require 'inputs/userinput'
require 'inputs/shootcoordinatesinput'

describe ShootCoordinatesInput do
  before(:all) do
    @inputs = {A1: [1, 1], A2: [2, 2], B3: [3, 3]}
    @input = ShootCoordinatesInput.new(inputs: @inputs)
  end

  context 'when user enter correct cell number for shoot' do
    it 'should print message "Please enter coordinates for shooting"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A1')
      expect(ConsoleOutput).to receive(:puts).with('Please enter coordinates for shooting')
      @input.fetch
    end

    it 'should return coordinate that have relationship with entered cell number' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A1')
      expect(@input.fetch).to be == @inputs[:A1]
    end
  end

  context 'when user enter wrong cell number for shoot' do
    it 'should print message "Incorrect coordinates, please enter coordinates in range A1-J10"' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A3', 'A2')
      expect(ConsoleOutput).to receive(:puts).with('Incorrect coordinates, please enter coordinates in range A1-J10')
      @input.fetch
    end

    it 'must require correct input from user' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleOutput).to receive(:puts)
      allow(ConsoleInput).to receive(:gets).and_return('A3', 'B3')
      expect(@input.fetch).to be == @inputs[:B3]
    end
  end
end
