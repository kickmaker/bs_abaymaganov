require 'console_wrap/consoleinput'
require 'console_wrap/consoleoutput'
require 'inputs/someinput'
require 'inputs/userinput'

describe UserInput do
  before(:all) do
    @inputs = {A1: [1, 1], A2: [2, 2], B3: [3, 3]}
    @input = UserInput.new(inputs: @inputs, case_sensitive: false)
  end

  context 'when user enter correct cell number' do
    it 'should return value that have relationship with entered cell number' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleInput).to receive(:gets).and_return('A1')
      expect(@input.fetch).to be == @inputs[:A1]
    end
  end

  context 'when user enter wrong string' do
    it 'should return nil' do
      allow(ConsoleOutput).to receive(:print)
      allow(ConsoleInput).to receive(:gets).and_return('A3')
      expect(@input.fetch).to be_nil
    end
  end
end
