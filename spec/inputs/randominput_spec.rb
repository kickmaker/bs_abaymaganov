require 'inputs/someinput'
require 'inputs/randominput'

describe RandomInput do
  before(:all) do
    @inputs = [[1, 1], [2, 2], [3, 3]]
    @input = RandomInput.new(inputs: @inputs)
  end

  context 'when fetch calls' do
    it 'should return any value from inputs' do
      value = @input.fetch
      expect(@inputs).to include value
    end
  end
end
