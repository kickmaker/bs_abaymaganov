require 'coordinates'
require 'cells/cell'

describe Cell do
  before(:all) do
    @cell = Cell.new Coordinates.new(0, 0)
  end

  context 'when initialized' do
    it 'have position (0, 0)' do
      expect(@cell.position).to be == Coordinates.new(0, 0)
    end

    it 'have state "unchecked"' do
      expect(@cell.state).to be == :unchecked
    end
  end

  context 'when check_it!' do
    it 'have state "checked"' do
      @cell.check_it!
      expect(@cell.state).to be == :checked
    end
  end
end
