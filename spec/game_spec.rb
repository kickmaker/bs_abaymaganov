require 'libloader'

describe Game do
  let(:game) { Game.new }

  describe '#configure' do
    context 'when game configured' do
      it 'should create GameOptionsInput with args'
      it 'should call GameOptionsInput.fetch once'
    end
  end

  describe '#prepare' do
    context 'when game prepared' do
      it 'should create player Board with options'
      it 'should create enemy Board with options'
      it 'should call Board.fill_with_ship! twice'
      it 'should create RealPlayer with enemy board'
      it 'should create VirtualPlayer with user board'
    end
  end

  describe '#play' do
    context 'when game started' do
      context 'and player plays' do
        it 'should call RealPlayer.shoot'

      end

      context 'and enemy plays' do

      end

      context 'and unknown player role' do
        it 'should fails with message \'Undefined player role\''
      end
    end
  end
end
