require 'coordinates'

describe Coordinates do
  context 'when coordinates initialize' do
    it 'x must be equal to 1, y must be equal to 2' do
      coord = Coordinates.new(1, 2)
      expect(coord.x).to be == 1
      expect(coord.y).to be == 2
    end
  end

  context 'when compare two coordinates' do
    it 'must be not equal' do
      coord1 = Coordinates.new(0, 0)
      coord2 = Coordinates.new(1, 1)
      expect(coord1).not_to be == coord2
    end

    it 'must be equal' do
      coord1 = Coordinates.new(1, 1)
      coord2 = Coordinates.new(1, 1)
      expect(coord1).to be == coord2
    end
  end
=begin
  context 'when coordinates within array' do
    it 'are compared with each other' do
      arr = Array.new(3) {|i| Coordinates.new(i, i)}

    end
  end
=end
end
