/**
 * Created by dogmat on 14.03.16.
 */

function Coordinates(x, y) {
// use it instead properties
    this.x = function() {
        return this._x;
    };

    this.y = function() {
        return this._y;
    };

// private propertirs
    this._x = x;
    this._y = y;
}
