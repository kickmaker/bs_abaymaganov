class VirtualPlayer < SomePlayer
  def initialize(board)
    super(board)

    @possible_inputs = @board.field.select do |cell|
      cell.state == :unchecked
    end.map { |cell| cell.position }

    @recommended_inputs = @possible_inputs
    @hits_sequence = []
  end

  def shoot
    coord = get_coord
    ConsoleOutput.puts("\nEnemy shoots to #{ ('A'.ord + coord.x).chr + (coord.y + 1).to_s }")
    was_hit = @shooter.shoot(coord)

    if was_hit
      @hits_sequence << coord

      strategy = create_strategy(@board.placing, @hits_sequence, 4)
      @recommended_inputs = strategy.places.select do |place|
        @possible_inputs.include?(place)
      end

      ConsoleOutput.puts("Enemy hit!\n\n")
    else
      ConsoleOutput.puts("Enemy missed\n\n")
    end

    # when destroy ship
    if @recommended_inputs.empty?
      @hits_sequence.each { |coord| @possible_inputs -= Ship::cell_surrounding(coord) }
      @hits_sequence.clear
      @recommended_inputs = @possible_inputs
    end

    was_hit
  end

  private

  include Strategies

  def get_coord
    input = RandomInput.new(inputs: @recommended_inputs).fetch
    @recommended_inputs -= [input]
    @possible_inputs -= [input]
    input
  end
end
