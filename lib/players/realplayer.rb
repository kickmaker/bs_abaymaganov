class RealPlayer < SomePlayer
  def initialize(board)
    super(board)

    possible_inputs = player_possible_inputs.merge(Game::CHEATS).merge(Game::EXIT)
    @input = ShootCoordinatesInput.new(inputs: possible_inputs)
  end

  def shoot
    was_hit = @shooter.shoot(get_coord)
    ConsoleOutput.puts("You hit!\n\n") if was_hit
    ConsoleOutput.puts("You missed\n\n") unless was_hit
    was_hit
  end

  private

  include PossibleInputs::Player

  def get_coord
    input = @input.fetch
    raise UndefinedUserInput.new(input) unless input.is_a? Coordinates
    input
  end
end
