class Ship
  attr_reader :coords

  def self.cell_surrounding(coord)
    surrounding = []

    (-1..1).each do |i|
      (-1..1).each do |j|
        surrounding << Coordinates.new(coord.x + i, coord.y + j)
      end
    end

    surrounding.delete_if do |coordinate|
      (coordinate.x < 0) || (coordinate.x > 9) ||
        (coordinate.y < 0) || (coordinate.y > 9) || (coordinate == coord)
    end
  end

  def initialize(coords)
    @coords = coords
  end

  def filled_space
    space = []
    @coords.each { |coord| space |= (Ship.cell_surrounding(coord) << coord) }
    space
  end
end
