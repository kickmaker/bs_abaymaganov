class Game
  TYPE = {
    auto: 0,
    manual: 1
  }.freeze

  RULES = {
    default: 2,
    tetris: 3
  }.freeze

  START = {
    go: 4
  }.freeze

  EXIT = {
    quit: 5
  }.freeze

  CHEATS = {
    surrender: 6
  }.freeze

  ROLES = {
      player: 0,
      enemy: 1
  }.freeze

  DECKS_COUNT = 20

  def configure
    greeting

    commands = TYPE.merge(RULES).merge(START).merge(EXIT)
    default_options = { input_type: TYPE[:auto], placing_rules: RULES[:default] }

    input = GameOptionsInput.new(inputs: commands, case_sensitive: true, default_options: default_options)
    @player_board_options = input.fetch

    @enemy_board_options = {
        input_type: TYPE[:auto],
        placing_rules: @player_board_options[:placing_rules]
    }
  end

  def prepare
    @player_hits_counter = HitsCounter.new(DECKS_COUNT)
    @enemy_hits_counter = HitsCounter.new(DECKS_COUNT)

    @player_board = Board.new(@player_board_options)
    @player_board.fill_with_ships!

    @enemy_board = Board.new(@enemy_board_options)
    @enemy_board.fill_with_ships!

    @player = RealPlayer.new(@enemy_board)
    @enemy = VirtualPlayer.new(@player_board)

    @role = ROLES[:enemy]
  end

  def play
    ConsoleOutput.puts('!!!GAME STARTED!!!')

    loop do
      case @role
      when ROLES[:player]
        begin
          was_hit = @player.shoot

        rescue UndefinedUserInput => exception
          case exception.input
          when CHEATS[:surrender]
            surrender
            ConsoleOutput::puts('What a shame...')
            break
          when EXIT[:quit]
            ConsoleOutput::puts('Game closed...')
            break
          else
            fail exception.inspect
          end
        end

        @player_hits_counter.count_up! { was_hit }
        show_boards

        if @player_hits_counter.reached?
          ConsoleOutput.puts('You win!')
          break
        end

        @role = ROLES[:enemy] unless was_hit

      when ROLES[:enemy]
        was_hit = @enemy.shoot

        @enemy_hits_counter.count_up! { was_hit }
        show_boards
        sleep(1)

        if @enemy_hits_counter.reached?
          ConsoleOutput.puts('You lose...')
          break
        end

        unless was_hit
          ConsoleOutput.puts("\nYour turn")
          @role = ROLES[:player]
        end
      else
        fail 'Undefined player role'
      end
    end
  end

  private

  def greeting
    greet = "Welcome to Battle Sea The Game\n\n"
    greet << "--------------------------------=MENU=-------------------------------------\n\n"
    greet << "To choose placing rules press: default - for standard placing rules\n"
    greet << "                               tetris  - for tetris placing rules\n\n"
    greet << "To choose placing mode press: auto   - for auto ships placing\n"
    greet << "                              manual - for manual ships placing\n\n"
    greet << "Press \"quit\" to close game\n\n"
    greet << "Press \"go\" to start game\n\n"
    greet << "---------------------------------------------------------------------------\n\n"

    ConsoleOutput.puts(greet)
  end

  def show_boards
    @presenter ||= BoardsPresenter.new(@player_board, @enemy_board)
    @presenter.present
  end

  def surrender
    @presenter ||= BoardsPresenter.new(@player_board, @enemy_board)
    @presenter.present(surrender: true)
  end
end
