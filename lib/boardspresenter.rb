class BoardsPresenter
  def initialize(player_board, enemy_board = nil)
    @player_board = player_board
    @enemy_board = enemy_board
  end

  def present(options = {})
    rows = Board::ROWS
    cols = Board::COLS

    half_header = ' ' * rows.to_s.length
    (0..cols - 1).each { |i| half_header << ('A'.ord + i).chr }
    header = half_header
    header << '  ' + half_header if @enemy_board
    ConsoleOutput.puts(header)

    (0..rows - 1).each do |j|
      row_num = j + 1
      space_cnt = rows.to_s.length - row_num.to_s.length

      line = (' ' * space_cnt) + row_num.to_s
      (0..cols - 1).each do |i|
        cell = @player_board.field[i * cols + j]

        line << '.' if (cell.class == Cell) && (cell.state == :unchecked)
        line << '0' if (cell.class == Cell) && (cell.state == :checked)
        line << 's' if (cell.class == Deck) && (cell.state == :unchecked)
        line << 'x' if (cell.class == Deck) && (cell.state == :hit)
      end

      if @enemy_board
        space_between = '  '
        line << space_between << (' ' * space_cnt) + row_num.to_s

        (0..cols - 1).each do |i|
          cell = @enemy_board.field[i * cols + j]

          if options[:surrender]
            line << '.' if (cell.class == Cell) && (cell.state == :unchecked)
            line << '0' if (cell.class == Cell) && (cell.state == :checked)
            line << 's' if (cell.class == Deck) && (cell.state == :unchecked)
            line << 'x' if (cell.class == Deck) && (cell.state == :hit)
          else
            line << '.' if cell.state == :unchecked
            line << '0' if (cell.class == Cell) && (cell.state == :checked)
            line << 'x' if (cell.class == Deck) && (cell.state == :hit)
          end
        end
      end

      ConsoleOutput.puts(line)
    end
  end
end
