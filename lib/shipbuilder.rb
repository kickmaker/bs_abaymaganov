class ShipBuilder
  def initialize(deck_size, board, possible_choices, options)
    @deck_size = deck_size
    @board = board
    @possible_choices = possible_choices
    @input_type = options[:input_type]
    @placing_rules = options[:placing_rules]
  end

  def try_to_place!
    return false if @deck_size < 1
    return false if @possible_choices.empty?

    possible_choices = @possible_choices
    begin
      # choose first cell within possible_choices
      coord = get_good_coord(@input_type, possible_choices)

      # build input variations tree for chosen coordinate
      tree_builder_options = {
          :root => coord,
          :possible_choices => @possible_choices,
          :placing_rules => @placing_rules,
          :decks => @deck_size
      }
      tree_builder = InputVariationsTreeBuilder.new(tree_builder_options)
      variations_tree = tree_builder.build

      # when too little space for the ship with deck_size in chosen coordinate
      can_built_ship = (variations_tree.height + 1) >= @deck_size
      unless can_built_ship
        possible_choices.delete(coord)
        return false if possible_choices.empty?

        if @input_type == Game::TYPE[:manual]
          ConsoleOutput.puts('Bad place for ship, please choose again')
        end
      end
    end until can_built_ship

    # choose other coordinates for ships and push it to decks
    cur_node = variations_tree.root
    decks = [coord]
    @deck_size.pred.times do
      next_nodes = cur_node.next_nodes

      next_choices = next_nodes.select do |node|
        is_good_branch = node.height == variations_tree.height - node.depth
        is_good_choose = possible_choices.include? node.coordinates

        is_good_branch && is_good_choose
      end.map(&:coordinates)

      unless @input_type == Game::TYPE[:auto]
        message = 'Enter one of next possible coordinates: '
        next_choices.each { |crd| message << ('A'.ord + crd.x).chr + (crd.y + 1).to_s + ', ' }
        ConsoleOutput.puts(message.chop.chop)
      end

      next_coord = get_good_coord(@input_type, next_choices)
      decks << next_coord
      next_nodes.each { |node| cur_node = node if node.coordinates == next_coord }
    end

    Ship.new(decks)
  end

  private

  include PossibleInputs::Player

  # get good coordinates from input class
  def get_good_coord(input_type, possible_choices)
    return nil if possible_choices.empty?

    input = case input_type
              when Game::TYPE[:auto]
                RandomInput.new(inputs: possible_choices)
              when Game::TYPE[:manual]
                options = { inputs: player_possible_inputs, choices: possible_choices }
                DeckCoordinatesInput.new(options)
              else
                fail 'Undefined input type'
            end

    input.fetch
  end
end
