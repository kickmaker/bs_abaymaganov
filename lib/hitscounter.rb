class HitsCounter
  attr_reader :count

  def initialize(limit)
    @limit = limit
    @count = 0
  end

  def count_up!
    return unless block_given?
    @count += 1 if yield
  end

  def reached?
    @count >= @limit
  end
end
