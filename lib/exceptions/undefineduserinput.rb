class UndefinedUserInput < Exception
  attr_reader :input

  def initialize(input = nil)
    @input = input
    super('Undefined user input')
  end
end
