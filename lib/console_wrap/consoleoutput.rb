class ConsoleOutput
  def self.puts(mess)
    Kernel.puts(mess)
  end

  def self.print(mess)
    Kernel.print(mess)
  end
end
