class Shooter
  def initialize(board)
    @board = board
  end

  def shoot(coord)
    index = @board.field.index { |cell| cell.position == coord }

    if index
      state_before = @board.field[index].state
      state_after = @board.field[index].check_it!
      is_state_changed = (state_before != state_after)

      return is_state_changed && (@board.field[index].state == :hit)
    end

    false
  end
end
