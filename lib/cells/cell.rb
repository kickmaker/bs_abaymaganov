class Cell
  attr_reader :state, :position

  def initialize(pos)
    fail 'Incorrect initialization parameters' unless pos.is_a? Coordinates

    @state = :unchecked
    @position = pos
  end

  def check_it!
    @state = :checked
  end
end
