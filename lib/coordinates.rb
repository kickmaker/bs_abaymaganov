class Coordinates
  attr_accessor :x, :y

  def initialize(x, y)
    @x = x
    @y = y
  end

  def ==(coord)
    return super(coord) unless coord.is_a? Coordinates
    (@x == coord.x) && (@y == coord.y)
  end

  def hash
    (@x.to_s + @y.to_s).to_i
  end

  def eql?(other)
    (object_id == other.object_id) || (self == other)
  end
end
