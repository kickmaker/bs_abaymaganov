module Strategies
  def create_strategy(placing_rules, positions, decks)
    strategy = case placing_rules
                 when Game::RULES[:default]
                   DefaultStrategy.new(positions, decks)
                 when Game::RULES[:tetris]
                   HellTetrisStrategy.new(positions, decks)
                 else
                   fail 'Undefined placing rules'
               end
  end
end
