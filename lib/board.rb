class Board
  ROWS = 10
  COLS = 10

  attr_reader :field

  def self.clean_outer(coords)
    clean_coords = Array.new(coords)
    clean_coords.delete_if do |coord|
      (coord.x < 0) || (coord.x > 9) || (coord.y < 0) || (coord.y > 9)
    end
  end

  def initialize(options)
    @ships = { 4 => 1, 3 => 2, 2 => 3, 1 => 4 }

    @field = Array.new(COLS) do |x|
      Array.new(ROWS) do |y|
        Cell.new(Coordinates.new(x, y))
      end
    end.flatten(1)

    @options = options
  end

  def placing
    @options[:placing_rules]
  end

  def fill_with_ships!
    possible_choices = Array.new(Board::COLS) do |x|
      Array.new(Board::ROWS) do |y|
        Coordinates.new(x, y)
      end
    end.flatten(1)

    is_manual_placing = @options[:input_type] == Game::TYPE[:manual]
    presenter = BoardsPresenter.new(self)

    @ships.each_pair do |deck_size, count|
      count.times do
        if is_manual_placing
          presenter.present(surrender: true)
          ConsoleOutput.puts("Please enter coordinates for #{ deck_size.to_s }-decker ship")
        end

        builder = ShipBuilder.new(deck_size, self, possible_choices, @options)
        ship = builder.try_to_place!

        fail 'Ship wasn\'t built' unless ship

        ship.coords.each do |deck_coord|
          i = @field.index { |cell| cell.position == deck_coord }
          @field[i] = Deck.new(deck_coord)
        end

        possible_choices -= ship.filled_space
        ConsoleOutput.puts('Ship have been set to board') if is_manual_placing
      end
    end

    ConsoleOutput.puts('All ships have been set') if is_manual_placing
  end
end
