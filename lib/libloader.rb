lib_dir = File.dirname(__FILE__)

# update $LOAD_PATH
$LOAD_PATH.unshift(lib_dir)
Dir[lib_dir + '/*/'].each { |dir| $LOAD_PATH.unshift(dir) }

# load requirements
requirements = Dir[lib_dir + '/**/*.rb'].map { |file| File.basename(file, File.extname(file)) }

loop do
  requirements = requirements.delete_if do |file|
    begin
      require file
      true
    rescue
      false
    end
  end

  break if requirements.empty?
end
