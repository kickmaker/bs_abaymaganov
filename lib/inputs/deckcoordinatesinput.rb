class DeckCoordinatesInput < UserInput
  def initialize(options)
    @choices = options[:choices]
    super(options)
  end

  def fetch
    loop do
      coord = super

      unless coord
        ConsoleOutput.puts('Incorrect coordinates, please enter coordinates in range A1-J10')
        next
      end

      unless @choices.include? coord
        ConsoleOutput.puts('Bad place for deck, please choose again')
        next
      end

      return coord
    end
  end
end
