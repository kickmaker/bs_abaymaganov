module PossibleInputs
  module Player
    # return hash {:A0 => (0, 0), :A1 => (0, 1), ... etc}
    def player_possible_inputs
      possible_inputs = {}

      (0..9).each do |x|
        (0..9).each do |y|
          cell_name = ('A'.ord + x).chr + (y + 1).to_s
          possible_inputs[cell_name] = Coordinates.new(x, y)
        end
      end

      possible_inputs
    end
  end
end
