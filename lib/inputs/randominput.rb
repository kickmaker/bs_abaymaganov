class RandomInput < SomeInput
  def fetch
    @inputs.sample
  end
end
