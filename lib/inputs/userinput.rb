class UserInput < SomeInput
  def initialize(options)
    @case_sensitive = options[:case_sensitive]
    @case_sensitive = false unless @case_sensitive
    super(options)
  end

  def fetch
    ConsoleOutput.print('> ')
    input = ConsoleInput.gets.strip.chomp
    input = input.upcase unless @case_sensitive

    @inputs.each_key do |k|
      key = k.to_s
      key = key.upcase unless @case_sensitive

      return @inputs[k] if input == key
    end

    nil
  end
end
