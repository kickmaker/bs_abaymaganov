class GameOptionsInput < UserInput
  def initialize(options)
    super(options)
    @options = options[:default_options]
    @options = {} unless @options
  end

  def fetch
    loop do
      command = super

      case command
      when Game::TYPE[:auto]
        ConsoleOutput.puts('Ship auto placing have been activated')
        @options[:input_type] = command
      when Game::TYPE[:manual]
        ConsoleOutput.puts('Ship manual placing have been activated')
        @options[:input_type] = command
      when Game::RULES[:default]
        ConsoleOutput.puts('Standard game rules have been activated')
        @options[:placing_rules] = command
      when Game::RULES[:tetris]
        ConsoleOutput.puts('Tetris game rules have been activated')
        @options[:placing_rules] = command
      when Game::EXIT[:quit]
        ConsoleOutput.puts('Game closed...')
        exit
      when Game::START[:go]
        return @options
      else
        ConsoleOutput.puts('Unknown command, please repeat...')
      end
    end
  end
end
