class ShootCoordinatesInput < UserInput
  def fetch
    loop do
      ConsoleOutput.puts('Please enter coordinates for shooting')
      coord = super
      return coord if coord
      ConsoleOutput.puts('Incorrect coordinates, please enter coordinates in range A1-J10')
    end
  end
end
